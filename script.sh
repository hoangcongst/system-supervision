#!/bin/bash
function too_many_connect_fail() {
	local numFailed=$(grep -c "Failed password" /var/log/auth.log)
	if [ $numFailed = 1 ]; then
		echo attacks
		 echo $HOST_NAME "is under SSH attacks" | mail -s "System Issues" hoangcongst@gmail.com
	fi
}

function cpu_over() {
	local cpu_usage=$(top -b -n1 | grep "Cpu(s)" | awk '{print $2 + $4}')
	if [ $cpu_usage > 5 ]; then
		echo cpu over
		echo "CPU over at " $(date +"%A, %B %-d, %Y") | mail -s "System Issues" hoangcongst@gmail.com
	fi
}

function opened_port() {
	echo hello
}

too_many_connect_fail
cpu_over
opened_port

